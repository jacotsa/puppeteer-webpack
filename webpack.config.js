
var webpack = require('webpack');
module.exports = env => {
    console.log("env", env);
    console.log("env", env);
    console.log("env", env);
    console.log("env", env);
    return {
        devServer: {
            disableHostCheck: true,
            host: '0.0.0.0',
            port: 80
        },
        plugins: [
            new webpack.DefinePlugin({
                USERNAME: env.USERNAME || 'notfound', // default value if not specified
                PASSWORD: env.PASSWORD || 'PASSWORD'
            })
        ]
    }
}
